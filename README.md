# LoRa
Rola,Sx1276 测试


# DIO 配置
空闲的时候是低电平,发送,接收均是从低电平到高,且保持不变,只有切换模式才会变回低电平

# crc待确认
字节带crc,n字节为4+4+N+2,4个Preamble+4个同步字+2个CRC校验,这个可设置

# standby模式
发送和接收切换必须经过standby模式

# 长数据包发送
```
void SX1276_TxPacket(INT8U *dat)
{
    SX1276Write( REG_LR_OPMODE, 0x80|RFLR_OPMODE_STANDBY );      
    SX1276Write( REG_LR_PREAMBLEMSB,0);//前导码
    SX1276Write( REG_LR_PREAMBLELSB,10);
    SX1276Write( REG_LR_PAYLOADLENGTH,G_LoRaConfig.PayloadLength);                    
    //SX1276WriteRxTx(TRUE);  
	SX1276Write( REG_LR_FIFOTXBASEADDR,0x00); //长数据包发送注意这个                                  
    SX1276Write( REG_LR_FIFOADDRPTR,0x00);                      
    SX1276WriteBuffer(REG_LR_FIFO,dat,G_LoRaConfig.PayloadLength);                   
    SX1276Write(REG_LR_IRQFLAGS,0xff);                          
    SX1276Write( REG_LR_IRQFLAGSMASK, ~(RFLR_IRQFLAGS_TXDONE)); 
    SX1276Write( REG_LR_DIOMAPPING1, RFLR_DIOMAPPING1_DIO0_01 );//DIO配置
    SX1276Write( REG_LR_OPMODE, 0x80|RFLR_OPMODE_TRANSMITTER );
}
```

# 待更改
 - read,write 禁止中断